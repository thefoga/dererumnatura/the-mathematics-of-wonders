---
name: Feature request
about: Suggest an idea for this project

---

Summary.

## Wanted feature

What you expect.

## Chapter

Is it a feature that is relevant just to one of the book chapter? Which one?
