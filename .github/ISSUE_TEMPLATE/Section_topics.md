---
name: Multiple new topics in a specific chapter
about: Please add the following topics to a specific chapter

---

## Section

What is the section the topic should be found in

| title | small description (< 140 chars) | links, references |
| --------- | --------- | --------- |
| | | | 
| | | | 
| | | | 
