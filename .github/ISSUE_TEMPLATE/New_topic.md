---
name: New topic
about: Create a topic about something amazing, e.g primes in Mandelbrot set

---

Summary.

## Title

What the title should be

## Section

What is the section the topic should be found in

## How did you discover it?

Extra links and references