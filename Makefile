cwd            := $(shell pwd)
scripts_folder := "scripts/"
update         := sudo apt -qq update
install        := sudo apt install -y --no-install-recommends

.PHONY: all view

view:
	xdg-open Book.pdf

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof *.dvi *.bcf *.run* # root
	rm -fv */*.aux */*.log */*.bbl */*.blg */*.toc */*.out */*.lot */*.lof */*.dvi */*.bcf */*.run*
	rm -fv */*/*.aux */*/*.log */*/*.bbl */*/*.blg */*/*.toc */*/*.out */*/*.lot */*/*.lof */*/*.dvi */*/*.bcf */*/*.run*

install::
	$(update)                                                              && \
	$(install) texlive-fonts-recommended texlive-latex-extra               && \
	$(install) texlive-fonts-extra dvipng texlive-latex-recommended        && \
	$(install) hunspell hunspell-it hunspell-en-gb myspell-fr                && \
	$(install) texlive-bibtex-extra biber # biblatex

build::
	pdflatex Book
	biber Book
	pdflatex Book
	pdflatex Book

debug::
	$(MAKE) build
	$(MAKE) view

spell::
	cd $(scripts_folder)                                                   && \
	bash spell_check.sh
