\chapter{Approximations or The Art of Survival}
\label{ch:Approximations}
Maybe the title of this chapter would ashame pure mathematicians, but even if it's hard to  believe, approximation formulas appear even in the most pure and aesthetic of the arts, Mathematics.\\
Besides, it's not even said that one day in the future these formulas, will become proved and exact facts with a $=$ at the place of $\approx$ \ldots who knows?

\section{Leibniz and Gregory}
\label{sec:Leibniz and Gregory}
Monks are not just \textit{monks} \ldots cm'on! They are always on books, reading who-knows-what: they must have great minds and that's exactly what happened to James Gregory (and other ones). However, before all, let's make a little background\ldots\\
Most of the readers might have encountered functions like $x, x^2, \frac{x^3}{6} \ldots$; less may have come across something like $\frac{1}{x}, \frac{2}{x^3 + 1}, e^x \ldots$; and fewer might have seen $\cos(x), \arctan(x), \ln(x), \int _0 ^x f(t) \ dt \ldots$; maybe 1 out of 100 might have heard about $\ln(\sinh(x)^x), \sum_{n=0} ^\infty a^n \cos(b^n \pi x), \frac{1}{2 \pi} \int_{-\infty} ^{+\infty} e^{ip (x-a)} \ dp \ldots$.
The point is that not all function are \textit{easy} to calculate or conceive like the first ones; there are also non-computable functions or even functions with non-\hyperref[sec:A note on notations (cit.)]{closed-form} formula. Yet approximations of these difficult functions are possible with the well known Taylor series.\\
The reader should be advised that not all functions can have \textit{approximations}, but when this happens, it's very common to study why there are those approximations, and how we can exploit them. In calculus a function $f(x)$ is said to be analytic when it is locally given by a convergent power series, so it means it can have a similar function that is easier to express and to calculate, and it is a very nice approximation of the first function in that point. Adding more terms to the expansion gives a more precise approximation of the function.\\
But what about monks? As you can imagine finding a function that is an approximation of another one is a difficult task. Despite that the highly skilled mind of James Gregory found the approximation-function (series) of the function $\arctan(x)$ in 1761:
$$
\begin{aligned}\arctan(x) &= \sum\limits_{k=1}^{+\infty} \frac{(-1)^{k-1}}{(2k-1)} \cdot x^{2k-1}\\
&= \frac{1}{x} - \frac{1}{3x^3} + \frac{1}{5x^5} - \frac{1}{7x^7} + \ldots
\end{aligned}
$$

This is the way calculators work. Instead of calculating a difficult function such as $\arctan(x)$, they make approximations and calculate $\frac{1}{x} - \frac{1}{3x^3} + \frac{1}{5x^5} - \frac{1}{7x^7} + \ldots$ 10/15 terms, in order to give an exact result up to the 10-th/15-th digit.\\
James Gregory has been very clever and insightful, but he missed an opportunity that costed him a formula. Now this formula is called \textbf{Leibniz serie} and it has been found by Leibniz by simply substituting $x=1$ in Gregory's $\arctan(x)$ formula:
$$\arctan(1) = \frac{\pi}{4} = 1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \frac{1}{9} + \ldots$$
which means that
$$\boxed{\pi = 4 \lim_{n\to +\infty} \sum\limits_{k=1}^{n} \frac{(-1)^{k-1}}{(2k-1)} \cdot x^{2k-1}}$$

\section{Partitions}
\label{sec:Partitions}
Consider a positive integer $n$ and its non-ordered partitions. Denote with $p(n)$ the number of such partitions, $\forall n \in \mathbb{N}$. Then
$$\boxed{p(n) \approx \frac{e^{\pi \sqrt{\frac{2n}{3}}}}{4n\sqrt{3}}}$$
For example, let's take into consideration the case $n=4$; the equation beforehand just told us that
$$p(4) \approx \frac{e^{\pi \sqrt{\frac{8}{3}}}}{16\sqrt{3}} \approx 6.1$$
and with no surprise we can assert that $4$ can be divided into 5 different ways:
$$
\begin{array}{c|lcr}
1 & 4 \\
2 & 3+1 \\
3 & 2+2 \\
4 & 2+1+1 \\
5 & 1+1+1+1 \\
\end{array}
$$

\section{Prime Number theorem}
\label{sec:Prime Number theorem}
You wouldn't believe this but  \ldots primes are like weeds! Of course this is not the mathematical-formal way to notify you of such an important fact, so let's get the theorem.\\
\begin{thm}{Prime number theorem}
$$
\begin{aligned}
\pi (n) &= \text{number of primes below } n \approx \frac{n}{\ln(n)}\\
&\implies \text{Proportion of primes} \approx \frac{1}{\ln(n)}\\
&\implies \text{Average distance between two consecutive primes} \approx \ln(n)\\
&\implies n-\text{th prime} \approx n\cdot \ln(n)
\end{aligned}
$$
\end{thm}

Actually there is an exact formula to compute the $n-$th prime, but since you have to compute $2^n$ terms, it is \textit{extremely} inefficient.

\section{Stirling's approximation}
\label{sec:Stirling's approximation}
The factorial function $x!$ is easy to understand, yet it keeps amazing mathematicians in many ways. How about calculate $x!$ for very large numbers $x \in \mathbb{N}$? The recursive way is too long and expensive! Besides you would not be able to even imagine the magnitude order of $(10 ^ 60)!$!\\
Luckily \cite{James_Stirling_Wikipedia} gave us a rather useful and precise approximation of the formula:
$$n! \approx \sqrt{2 \pi n} \cdot \left( \frac{n}{e} \right)^ n $$
\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{ch/approximations/img/stirling_approx.png}
\caption{$n!$ VS $\sqrt{2 \pi n} \cdot \left( \frac{n}{e} \right)^ n$ }
\end{figure}

\section{Harmonic numbers}
\label{sec:Harmonic numbers}
This section is about \textit{harmonic} numbers, which are a very special kind of numbers the reader must know about. However they involve a little tricky subjects, such as integrals, which can make things worse for the take-it-easy kindof reader. So let's keep a focused mind and start working magic.\\
Consider the function
$$\begin{cases}
f(x) = \frac{1}{x}\\
f(x) : \mathbb{R} \implies \mathbb{R}
\end{cases}$$
\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{ch/approximations/img/harmonics.png}
\caption{$y = f(x) = \frac{1}{x}, f(x) : \mathbb{R} \implies \mathbb{R}$ }
\end{figure}
and calculate the area of the \textbf{n-1} trapezia built by joining the points
$(k,f(k) = 1/k), k \in \mathbb{N}^+$.
It's easy to find that the desired area is

$$
\begin{aligned}
\text{height} \frac{\text{small base} + \text{big base}}{2} &= 1\cdot \frac{\frac{1}{1} + \frac{1}{2}}{2} + 1\cdot \frac{\frac{1}{2} + \frac{1}{3}}{2} + 1\cdot \frac{\frac{1}{3} + \frac{1}{4}}{2} + \ldots + 1\cdot \frac{\frac{1}{k-1} + \frac{1}{k}}{2}\\
&=\frac{1}{2} \left( 2\sum\limits_{k=2} ^{n} \frac{1}{k} + \frac{1}{1} + \frac{1}{n} \right)\\
&= \frac{1}{2} \left( 2\sum\limits_{k=1} ^{n+1} \frac{1}{k} - \frac{1}{1} - \frac{1}{n} \right)\\
&=\sum\limits_{k=1} ^{n+1} \frac{1}{k} -\frac{1}{2} -\frac{1}{2n}\\
&=\mathcal{H}_{n} -\frac{1}{2} -\frac{1}{2n}
\end{aligned}
$$

And now here it is the Integral formulas coming out:
$$\int_a ^b \frac{1}{x} \diff x = \ln(b) - \ln(a)$$
$$\forall a,b \ge 1 | a \le b$$
Which enables us to have a great insight in the meaning of the area found. Applying the geometrical definition of $\int f(x) \diff x$ (the area between the \textit{x} -axis and the function) we calculate the same area in the interval $[1;n], \forall n \in \mathbb{N}^+$:
$$
\begin{aligned}
\int_1 ^n \frac{1}{x} \diff x &= \ln(n) - \ln(1)\\
&=\ln(n)
\end{aligned}
$$
But as the trapezia begin to approach more and more the curve
$$
\begin{aligned}
\text{Area under the curve} &\approx \text{Area of trapezia}\\
\int_1 ^n \frac{1}{x} \ \diff x &\approx 1\cdot \frac{\frac{1}{1} + \frac{1}{2}}{2} + 1\cdot \frac{\frac{1}{2} + \frac{1}{3}}{2} + 1\cdot \frac{\frac{1}{3} + \frac{1}{4}}{2} + \ldots + 1\cdot \frac{\frac{1}{k-1} + \frac{1}{k}}{2}\\
\ln(n) &\approx \mathcal{H}_{n} -\frac{1}{2} -\frac{1}{2n}
\end{aligned}
$$
But since mathematicians look for perfection and approximation are avoided most of the time, a new number \hyperref[sec:A note on notations (cit.)]{$\gamma$} has been invented. So we can say
$$\boxed{\mathcal{H}_{n} =\ln(n) + \gamma + \frac{1}{2n}}$$
Moreover two great identities come from these definitions and discoveries:
$$\sum \limits_{k=1} ^n \mathcal{H}_{k} = (n+1) \cdot (\mathcal{H}_{n+1} -1)$$
$$\sum \limits_{k=1} ^{+\infty} \mathcal{H}_{k} = \lim_{n \to \infty} (n+1) \cdot (\mathcal{H}_{n+1} -1) = \ln(n!\cdot \sqrt{n}) + \frac{1}{4n} + \gamma \cdot \left(n + \frac{1}{2} \right)$$

\section{Euler-Mascheroni VS hyperbolas}
\label{sec:Euler-Mascheroni VS hyperbolas}
May the reader read \hyperref[sec:Gauss circle problem]{Gauss' Circle Problem} chapter before continuing. Or may the force be with the brave readers who want to know more knowing less. The author is pleased to introduce the reader a similar problem with a solution that has just been improved by the author himself. Let the plot below give the necessary visual aid:
$$y = f(x) = \frac{r}{x}, r \in \mathbb{N}, \mathbb{R} ^+ \implies \mathbb{R} ^+$$
\begin{figure}[ht!]
\centering
\includegraphics[width=90mm]{ch/approximations/img/euler_mascheroni.png}
\caption{$y = f(x) = \frac{r}{x}, r \in \mathbb{N}, \mathbb{R} ^+ \implies \mathbb{R} ^+$ }
\end{figure}
Now, the author would like the reader to imagine a way to count all the lattice points under the beforehand function, namely $f(x) = \frac{r}{x}$, in function of the parameter $r$.
Obviously an explicit formula should be the aim of this job, not a power serie or something even more complicated.\\
Let's take a look at the line $x=k, \forall k \in \mathbb{N} \and 1 \le k \le r$: this line intersects the function at the point $(k, r/k)$, which means the number of lattice points on the line and below the curve is just $1 + \Bigl\lfloor \frac{r}{k}\Bigr\rfloor$. Thus the number of lattice pints under the curve between the lines $x = 1$ and $x = r$ is
$$
\begin{aligned}
\sum \limits_{k=1} ^r 1 + \Bigl\lfloor \frac{r}{k}\Bigr\rfloor &= \sum \limits_{k=1} ^ r 1 + \sum \limits_{k=1} ^ r \Bigl\lfloor \frac{r}{k}\Bigr\rfloor\\
&=r + \sum \limits_{k=1} ^ r \Bigl\lfloor \frac{r}{k}\Bigr\rfloor
\end{aligned}
$$
Now the sharp and acute reader should notice that
$$
\begin{aligned}
\lfloor a \rfloor \le a \forall a \in \mathbb{R} &\implies \Bigl\lfloor \frac{r}{k}\Bigr\rfloor \le \frac{r}{k}\\
&\implies \sum \limits_{k=1} ^ r \Bigl\lfloor \frac{r}{k}\Bigr\rfloor \le \sum \limits_{k=1} ^ r \frac{r}{k}\\
&=r\cdot \sum \limits_{k=1} ^ r \frac{1}{k}\\
&=r \cdot \left( \ln(r) + \gamma - \frac{1}{2r} \right)
\end{aligned}
$$
So the number searched is
$$
\begin{aligned}
N(r) &\le r + r\cdot \left( \ln(r) + \gamma - \frac{1}{2r} \right)\\
&= r \cdot \left( \ln(r) +1 + \gamma \right)
\end{aligned}
$$
Obviously there is some error $e(r)$, but up to know 16/08/2015, it's the best approximation the author has given so far.