<div align="center">
	<h1>The Mathematics of Wonders</h1>
	<em>Fun facts, puzzles and tips about mathematics</em></br>

<a href="https://travis-ci.org/sirfoga/the-mathematics-of-wonders"><img src="https://travis-ci.org/sirfoga/the-mathematics-of-wonders.svg?branch=master"></a> <a href="https://github.com/sirfoga/the-mathematics-of-wonders/pulls"><img src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a> <a href="https://github.com/sirfoga/the-mathematics-of-wonders/issues"><img src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a> <a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/License-MIT-blue.svg"></a>



<a href="https://saythanks.io/to/sirfoga"><img src="https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg" alt="Say Thanks!" /></a>
</div>


## Use case
You want to learn interesting maths sides and you're curious. Use this.


## Install
```shell
$ make install
```


## Usage
```shell
$ make build
```
then open the `Book.pdf` file with any `pdf` viewer.


## Warning
This is a work in progress. I urge you to keep up to date.


## Got questions?

If you have questions or general suggestions, don't hesitate to submit
a new [Github issue](https://github.com/sirfoga/the-mathematics-of-wonders/issues/new).


## Contributing
[Fork](https://github.com/sirfoga/the-mathematics-of-wonders/fork) | Patch | Push | [Pull request](https://github.com/sirfoga/the-mathematics-of-wonders/pulls)


## Feedback
Suggestions and improvements are [welcome](https://github.com/sirfoga/the-mathematics-of-wonders/issues)!


## Authors

| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|
| [Stefano Fogarollo](https://sirfoga.github.io) |


## Thanks
Thanks to
- [Nicola Cisternino](https://github.com/ShiroAimai) for providing an awesome template for the `.tex` files


## License
[MIT License](https://opensource.org/licenses/MIT)
